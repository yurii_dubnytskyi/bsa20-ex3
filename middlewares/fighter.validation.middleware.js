// const { fighter } = require('../models/fighter');
const { validateFighter} = require('./validation/fighter.validation');

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  const { errors, isValid } = validateFighter(req.body);

  if (!isValid) {
    return res.status(400).json({
      error: true,
      message: errors,
    });
  }

  next();
};

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update

    const { errors, isValid } = validateFighter(req.body);

    if (!isValid) {
        return res.status(400).json({
        error: true,
        message: errors,
        });
    }

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;