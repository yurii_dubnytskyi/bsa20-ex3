const UserService = require('../../services/userService');

class Validator {
  isEmpty = (value) => {
    return value === '' ? true : false;
  };

  isNumber = (value) => {
    console.log("Value is --"+Number(value))
    return typeof(Number(value)) === "number";
  };

  isAtLeast = (value, min) => {
    console.log("Value is --"+Number(value))
    return Number(value) >= min;
  };

  isLessThan = (value, max) => {
  
    return Number(value) < max;
  };

  isEmail = (value) => {
    return /(\W|^)[\w.+\-]*@gmail\.com(\W|$)/gi.test(value);
  };

  isPhoneNumber = (value) => {
    return /^\+?3?8?(0\d{9})$/g.test(value);
  };

  isPhoneNumberHave = (value) =>{
    let user = UserService.search({phoneNumber:value})
    if(user){
      return true
    }
  }
  isEmailHave = (value) =>{
    let user = UserService.search({email:value})
    if(user){
      return true
    }
  }

  isLength = (value, { min, max }) => {
    if (min) {
      if (max) {
        return value.length >= min && value.lenght < max;
      }
      return value.length >= min;
    }
  };
}

module.exports = new Validator();