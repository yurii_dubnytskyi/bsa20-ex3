const { Router } = require('express');
const UserService = require('../services/userService');
const {createUserValid,updateUserValid,} = require('../middlewares/user.validation.middleware');
const { User } = require('../models/user');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();


router.get('/', (req, res) => {
  const users = UserService.getUsers();
  if (users) {
    res.json(users);
  } else {
    res.json(400).json('have no users');
  }
});

router.get('/:id', (req, res) => {
  const id = req.params.id;
  const foundUser = UserService.search({ id });
  console.log("here")
  if (foundUser) {
    res.json(foundUser);
  } else {
    res.status(404).json({ nouser: 'No user with such id' });
  }
});


router.post('/', createUserValid, (req, res) => {
  const user = new User(req.body);
  const result = UserService.create(user);
  if (result) {
    res.json(result);
  } else {
    res.status(400).json('error has occured');
  }
});


router.put('/:id', updateUserValid, (req, res) => {
  const id = req.params.id;
  const userInfo = req.body;
  const updatedUser = UserService.update(id, userInfo);
  if (updatedUser) {
    res.json(updatedUser);
  } else {
    res.status(404).json({ nouser: 'No user with such id' });
  }
});


router.delete('/:id', (req, res) => {
  const id = req.params.id;
  const deletedUser = UserService.remove(id);
  if (deletedUser) {
    res.json(deletedUser);
  } else {
    res.status(404).json({ nouser: 'No user with such id' });
  }
});

module.exports = router;