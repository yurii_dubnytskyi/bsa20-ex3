const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { Fighter } = require('../models/fighter');
const {createFighterValid,updateFighterValid,} = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res) => {
  const fighters = FighterService.getFighters();
  if (fighters) {
    res.json(fighters);
  } else {
    res.json(400).json('have no fighters');
  }
});

router.get('/:id', (req, res) => {
  const id = req.params.id;
  const foundFighter = FighterService.search({ id });
  if (foundFighter) {
    res.json(foundFighter);
  } else {
    res.status(404).json({ nofighter: 'No fighter with such id' });
  }
});

router.post('/', createFighterValid, (req, res) => {
  const fighter = new Fighter(req.body);
  const result = FighterService.create(fighter);
  if (result) {
    res.json(result);
  } else {
    res.status(400).json('error has occured');
  }
});

router.put('/:id', updateFighterValid, (req, res) => {
  const id = req.params.id;
  const fighterInfo = req.body;
  const updatedFighter = FighterService.update(id, fighterInfo);
  if (updatedFighter) {
    res.json(updatedFighter);
  } else {
    res.status(404).json({ nofighter: 'No fighter with such id' });
  }
});


router.delete('/:id', (req, res) => {
  const id = req.params.id;
  const deletedFighter = FighterService.remove(id);
  if (deletedFighter) {
    res.json(deletedFighter);
  } else {
    res.status(404).json({ nofighter: 'No fighter with such id' });
  }
});

module.exports = router;