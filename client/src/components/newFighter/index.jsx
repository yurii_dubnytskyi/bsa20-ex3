import React, { useState } from 'react';
import { Button, TextField } from '@material-ui/core';

import { createFighter } from '../../services/domainRequest/fightersRequest';

import './newFighter.css';

export default function NewFighter({ onCreated }) {
  const [name, setName] = useState();
  const [power, setPower] = useState();
  const [defense, setDefense] = useState();
  const [health, setHealth] = useState();

  const onNameChange = (event) => {
    setName(event.target.value);
  };

  const onPowerChange = (event) => {
    const value =
      event.target.value || event.target.value === 0
        ? event.target.value
        : null;
    setPower(value);
  };

  const onDefenseChange = (event) => {
    const value =
      event.target.value || event.target.value === 0
        ? event.target.value
        : null;
    setDefense(value);
  };
  const onHealthChange = (event) => {
    const value =
      event.target.value || event.target.value === 0
        ? event.target.value
        : null;
    setHealth(value);
  };


  const onSubmit = async () => {
      
      console.log(power+"--"+defense)
    const data = await createFighter({ name,health, power, defense });
    if (data && !data.error) {
      onCreated(data);
    }
  };

  return (
    <div id="new-fighter">
      <div>New Fighter</div>
      <TextField
        onChange={onNameChange}
        id="name"
        label="Name"
        placeholder="Name"
      />
      <TextField
        onChange={onPowerChange}
        id="power"
        label="Power"
        placeholder="Power"
        type="number"
      />
      <TextField
        onChange={onDefenseChange}
        id="defense"
        label="Defense"
        placeholder="Defense"
        type="number"
      />
      <TextField
        onChange={onHealthChange}
        id="health"
        label="Health"
        placeholder="Health"
        type="number"
      />
      <Button onClick={onSubmit} variant="contained" color="primary">
        Create
      </Button>
    </div>
  );
}